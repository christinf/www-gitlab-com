---
layout: markdown_page
title: "Azure DevOps Continuous Delivery Overview"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

**Azure DevOps**

For support of traditional Release Management, Azure DevOps provides many primitives which GitLab doesn’t yet have but is working on, such as Release Approval Gates and Release Auditing.

**GitLab**

GitLab offers Progressive Delivery capabilities such as Feature Flags, Review Apps and Deployment Scenarios (canar, incremental, etc.), which Azure DevOps does not offer.

